<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sessions".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $date
 * @property string $name
 * @property string $description
 * @property string $text
 * @property string $audio
 * @property string $text_file
 * @property string $video_url
 * @property string $video_code
 * @property integer $private
 * @property integer $state
 * @property string $created
 * @property string $edited
 * @property integer $creator
 * @property integer $editor
 */
class Session extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sessions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'date', 'name'], 'required'],
            [['event_id', 'private', 'state', 'creator', 'editor'], 'integer'],
            [['date', 'created', 'edited'], 'safe'],
            [['description', 'text', 'video_code'], 'string'],
            [['name', 'audio', 'text_file', 'video_url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'date' => 'Date',
            'name' => 'Name',
            'description' => 'Description',
            'text' => 'Text',
            'audio' => 'Audio',
            'text_file' => 'Text file',
            'video_url' => 'Video URL',
            'video_code' => 'Video Code',
            'private' => 'Private',
            'state' => 'State',
            'created' => 'Created',
            'edited' => 'Edited',
            'creator' => 'Creator',
            'editor' => 'Editor',
        ];
    }

    public function getEvent()
    {
        return $this->hasOne(\common\models\Event::className(), ['id' => 'event_id']);
    }

    public function getTags()
    {
        return $this->hasMany(\common\models\Tag::className(), ['id' => 'tag_id'])
            ->viaTable('sessions_has_tags', ['session_id' => 'id']);
    }

    public function setTags($tags)
    {
        SessionsHasTags::deleteAll(['session_id' => $this->id]);
        $values = [];

        foreach ($tags as $tag_id) {
            $values[] = [$this->id, $tag_id];
        }

        self::getDb()->createCommand()
            ->batchInsert(SessionsHasTags::tableName(), ['session_id', 'tag_id'], $values)->execute();
    }
}
