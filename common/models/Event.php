<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property integer $teacher_id
 * @property integer $place_id
 * @property string $name
 * @property string $date_start
 * @property string $date_finish
 * @property string $description
 * @property integer $private
 * @property integer $state
 * @property string $created
 * @property string $edited
 * @property integer $creator
 * @property integer $editor
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id', 'place_id', 'name', 'date_start', 'date_finish'], 'required'],
            [['teacher_id', 'place_id', 'private', 'state', 'creator', 'editor'], 'integer'],
            [['date_start', 'date_finish', 'created', 'edited'], 'safe'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacher_id' => 'Teacher',
            'place_id' => 'Place',
            'name' => 'Name',
            'date_start' => 'Date Start',
            'date_finish' => 'Date Finish',
            'description' => 'Description',
            'private' => 'Private',
            'state' => 'State',
            'created' => 'Created',
            'edited' => 'Edited',
            'creator' => 'Creator',
            'editor' => 'Editor',
        ];
    }

    public function getPlace()
    {
        return $this->hasOne(\common\models\Place::className(), ['id' => 'place_id']);
    }

    public function getTeacher()
    {
        return $this->hasOne(\common\models\Teacher::className(), ['id' => 'teacher_id']);
    }
}
