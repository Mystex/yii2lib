<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "teachers".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $state
 * @property string $created
 * @property string $edited
 * @property integer $creator
 * @property integer $editor
 */
class Teacher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teachers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['text'], 'string'],
            [['state', 'creator', 'editor'], 'integer'],
            [['created', 'edited'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'text' => 'Text',
            'state' => 'State',
            'created' => 'Created',
            'edited' => 'Edited',
            'creator' => 'Creator',
            'editor' => 'Editor',
        ];
    }
}
