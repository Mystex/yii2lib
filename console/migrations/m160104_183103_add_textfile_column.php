<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_183103_add_textfile_column extends Migration
{
    public function up()
    {
        $this->addColumn('sessions', 'text_file', 'VARCHAR(255) NOT NULL after text');
    }

    public function down()
    {
        echo "m160104_183103_add_textfile_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
