<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_081614_create_table_tags extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('tags', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
        ], $tableOptions);

        $this->createTable('sessions_has_tags', [
            'session_id' => $this->integer(),
            'tag_id' => $this->integer(),
        ], $tableOptions);


        $this->createIndex('idx-session_tag-post_id', 'sessions_has_tags', 'session_id');
        $this->createIndex('idx-session_tag-tag_id', 'sessions_has_tags', 'tag_id');

        $this->addForeignKey('fk-session_tag-post_id', 'sessions_has_tags', 'session_id', 'sessions', 'id', 'CASCADE');
        $this->addForeignKey('fk-session_tag-tag_id', 'sessions_has_tags', 'tag_id', 'sessions', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('tags');
        $this->dropTable('sessions_has_tags');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
