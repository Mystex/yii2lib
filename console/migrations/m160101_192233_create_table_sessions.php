<?php

use yii\db\Schema;
use yii\db\Migration;

class m160101_192233_create_table_sessions extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('sessions', [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'text' => $this->text(),
            'audio' => $this->string(),
            'video_url' => $this->string(),
            'video_code' => $this->text(),
            'private' => $this->smallInteger(),
            'state' => $this->smallInteger(),
            'created' => $this->dateTime(),
            'edited' => $this->dateTime(),
            'creator' => $this->integer(),
            'editor' => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('sessions');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
