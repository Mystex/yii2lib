<?php

use yii\db\Schema;
use yii\db\Migration;

class m160101_191152_create_table_teachers extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('teachers', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'text' => $this->text(),
            'state' => $this->smallInteger(),
            'created' => $this->dateTime(),
            'edited' => $this->dateTime(),
            'creator' => $this->integer(),
            'editor' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('teachers');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
