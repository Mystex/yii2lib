<?php

use yii\db\Schema;
use yii\db\Migration;

class m160101_185950_create_table_events extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('events', [
            'id' => $this->primaryKey(),
            'teacher_id' => $this->integer()->notNull(),
            'place_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'date_start' => $this->date()->notNull(),
            'date_finish' => $this->date()->notNull(),
            'description' => $this->text(),
            'private' => $this->smallInteger()->defaultValue(0),
            'state' => $this->smallInteger()->defaultValue(0),
            'created' => $this->dateTime(),
            'edited' => $this->dateTime(),
            'creator' => $this->integer(),
            'editor' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('events');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
