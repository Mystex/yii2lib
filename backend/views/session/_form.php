<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

use \common\models\Event;
use \common\models\Tag;

/* @var $this yii\web\View */
/* @var $model common\models\Session */
/* @var $model common\models\Tag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="session-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <? /*=$form->field($model, 'event_id')->textInput()*/ ?>

    <?php
    $model->event_id = $event->id;
    ?>

    <?= $form->field($model, 'event_id')->dropDownList(ArrayHelper::map(Event::find()->all(), 'id', 'name'), [
        'readonly' => true,
        'value' => $event->id
    ]) ?>

    <?= $form->field($model, 'tags')->checkboxList(ArrayHelper::map(Tag::find()->all(), 'id', 'name'), [
        'options' => [
            1 => [
                'checked' => true,
            ]
        ]
    ]) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $model->audio ? $model->audio : '' ?>
    <?= $form->field($model, 'audio')->fileInput() ?>

    <?= $model->text_file ? $model->text_file : '' ?>
    <?= $form->field($model, 'text_file')->fileInput() ?>

    <?= $form->field($model, 'video_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'video_code')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'private')->checkbox() ?>

    <?= $form->field($model, 'state')->checkbox() ?>

    <?//= $form->field($model, 'created')->textInput() ?>

    <?//= $form->field($model, 'edited')->textInput() ?>

    <?//= $form->field($model, 'creator')->textInput() ?>

    <?//= $form->field($model, 'editor')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
