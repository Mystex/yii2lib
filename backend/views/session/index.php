<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SessionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sessions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="session-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Session', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'event_id',
            'date',
            'name',
            'description:ntext',
            // 'text:ntext',
            // 'audio',
            // 'video_url:url',
            // 'video_code:ntext',
            // 'private',
            // 'state',
            // 'created',
            // 'edited',
            // 'creator',
            // 'editor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
