<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Session */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['event/index']];
$this->params['breadcrumbs'][] = ['label' => $event->name, 'url' => ['event/view', 'id' => $event->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="session-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'event_id' => $model->event_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'event_id' => $model->event_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            // 'event_id',
            [
                'label' => 'Event',
                'value' => $model->event->name,
            ],
            [
                'label' => 'Teacher',
                'value' => $model->event->teacher->name,
            ],
            'date',
            'name',
            'description:ntext',
            'text:ntext',
            'audio',
            'text_file',
            'video_url:url',
            'video_code:ntext',
            // 'private',
            [
                'label' => 'Private',
                'value' => $model->private ? 'Yes' : 'No',
            ],
            // 'state',
            [
                'label' => 'State',
                'value' => $model->state ? 'Yes' : 'No',
            ],
            'created',
            'edited',
            'creator',
            'editor',
        ],
    ]) ?>

</div>
