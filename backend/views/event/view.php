<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchSessionModel backend\models\SessionSearch */
/* @var $dataSessionProvider common\models\Session */
/* @var $model common\models\Event */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Teacher',
                'value' => $model->teacher->name,
            ],
            [
                'label' => 'Place',
                'value' => $model->place->name,
            ],
            'name',
            'date_start',
            'date_finish',
            'description:ntext',
            [
                'label' => 'Private',
                'value' => $model->private ? 'Yes' : 'No',
            ],
            [
                'label' => 'State',
                'value' => $model->state ? 'Yes' : 'No',
            ],
            'created',
            'edited',
            'creator',
            'editor',
        ],
    ]) ?>


    <p>
        <?= Html::a('Create Session', ['session/create/', 'event_id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataSessionProvider,
        'filterModel' => $searchSessionModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'event_id',
            'name',
            'date',
//            'description:ntext',
            // 'text:ntext',
            // 'audio',
            // 'video_url:url',
            // 'video_code:ntext',
             'private',
             'state',
             'created',
//             'edited',
            // 'creator',
            // 'editor',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'session',
            ],
        ],
    ]); ?>

</div>
