<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use \common\models\Place;
use \common\models\Teacher;

/* @var $this yii\web\View */
/* @var $model common\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'teacher_id')->dropDownList(ArrayHelper::map(Teacher::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'place_id')->dropDownList(ArrayHelper::map(Place::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_start')->textInput() ?>

    <?= $form->field($model, 'date_finish')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'private')->checkbox() ?>

    <?= $form->field($model, 'state')->checkbox() ?>

    <?//= $form->field($model, 'created')->textInput() ?>

    <?//= $form->field($model, 'edited')->textInput() ?>

    <?//= $form->field($model, 'creator')->textInput() ?>

    <?//= $form->field($model, 'editor')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
