<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'name',
            [
                'attribute' => 'teacher_id',
                'value' => 'teacher.name'
            ],
            [
                'attribute' => 'place_id',
                'value' => 'place.name'
            ],
            'date_start',
            // 'date_finish',
            // 'description:ntext',
            // 'private',
            // 'state',
             'created',
            // 'edited',
            // 'creator',
            // 'editor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
