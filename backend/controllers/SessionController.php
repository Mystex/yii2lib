<?php

namespace backend\controllers;

use Yii;
use common\models\Session;
use backend\models\SessionSearch;
use common\models\Event;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SessionController implements the CRUD actions for Session model.
 */
class SessionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Session models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SessionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Session model.
     * @param integer $id
     * @param integer $event_id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $event = Event::findOne(['id' => $model->event_id]);

        return $this->render('view', [
            'model' => $model,
            'event' => $event,
        ]);
    }

    /**
     * Creates a new Session model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $event_id
     * @return mixed
     */
    public function actionCreate($event_id)
    {
        $model = new Session();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->created = date('Y-m-d H:i:s');
            $model->creator = Yii::$app->user->identity->getId();
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $event = Event::findOne(['id' => $event_id]);

            return $this->render('create', [
                'model' => $model,
                'event' => $event,
            ]);
        }
    }

    /**
     * Updates an existing Session model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        // Старые значения на случай если новые файлы не будут загружены
        $oldAudio = $model->audio;
        $oldTextFile = $model->text_file;

        if ($model->load(Yii::$app->request->post())) {
            // Загрузка аудио
            if ($audio = \yii\web\UploadedFile::getInstance($model, 'audio')) {
                $fileName = $audio->baseName . '.' . $audio->extension;
                $path = Yii::getAlias('@frontend/web/upload/audio/') . $fileName;
                $audio->saveAs($path);
                $model->audio = 'upload/audio/' . $fileName;
            } else {
                $model->audio = $oldAudio;
            }

            // Загрузка текстового файла
            if ($text_file = \yii\web\UploadedFile::getInstance($model, 'text_file')) {
                $fileName = $text_file->baseName . '.' . $text_file->extension;
                $path = Yii::getAlias('@frontend/web/upload/text/') . $fileName;
                $text_file->saveAs($path);
                $model->text_file = 'upload/text/' . $fileName;
            } else {
                $model->text_file = $oldTextFile;
            }

            // Если передана ссылка на youtube,
            // сгенерировать код iframe
            if ($model->video_url != '') {
                $pattern = '/http(s)?:\/\/(www\.)?(youtube\.com|youtu\.be)\/(watch\?[a-zA-Z0-9\&\-\_\=\%]*v\=)?([a-zA-Z0-9\-]+)(\&[a-zA-Z0-9\&\-\_\=\%\#]*)?/i';
                $frame = '<iframe width="560" height="315" src="//www.youtube.com/embed/$5" frameborder="0" allowfullscreen></iframe>';
                $model->video_code = preg_replace($pattern, $frame, $model->video_url);
            } else {
                $model->video_code = '';
            }

            // Теги
            $data = Yii::$app->request->post();
            $model->tags = $data['Session']['tags'];

            $model->edited = date('Y-m-d H:i:s');
            $model->editor = Yii::$app->user->identity->getId();
            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $event = Event::findOne(['id' => $model->event_id]);

            return $this->render('update', [
                'model' => $model,
                'event' => $event,
            ]);
        }
    }

    /**
     * Deletes an existing Session model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $event_id
     * @return mixed
     */
    public function actionDelete($id, $event_id)
    {
        $this->findModel($id, $event_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Session model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Session the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Session::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
